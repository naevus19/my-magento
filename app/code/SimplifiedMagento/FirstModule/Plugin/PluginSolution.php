<?php

/**
 * Created by PhpStorm.
 * User: kamil
 * Date: 25.03.20
 * Time: 15:46
 */

namespace SimplifiedMagento\FirstModule\Plugin;

class PluginSolution
{
//    public function beforeSetName(\Magento\Catalog\Model\Product $subject, $name)
//    {
//        return "Before Plugin " . $name;
//    }
//
//    public function afterGetName(\Magento\Catalog\Model\Product $subject, $result)
//    {
//        return $result . " After Plugin";
//    }
//
//    public function aroundGetIdBySku (\Magento\Catalog\Model\Product $subject, callable $proceed, $sku)
//    {
//        echo "before proceed </br>";
//        $id = $proceed($sku);
//        echo $id . "</br> after proceed </br>";
//        return $id;
//    }
//
//    public function aroundGetName(\Magento\Catalog\Model\Product $subject, callable $proceed)
//    {
//
//        echo "before proceed";
//        $name = $proceed();
//        return $name . " After Proceed";
//    }
    public function beforeExecute(\SimplifiedMagento\FirstModule\Controller\Page\HelloWorld $subject)
    {
        echo "before execute sort order 66 </br>";
    }
    public function afterExecute(\SimplifiedMagento\FirstModule\Controller\Page\HelloWorld $subject)
    {
        echo "after execute sort order 66</br>";
    }
    public function aroundExecute(\SimplifiedMagento\FirstModule\Controller\Page\HelloWorld $subject, callable $proceed)
    {
        echo "before proceed sort order 66 </br>";
        $proceed();
        echo "<br/>after proceed sort order 66 </br>";
    }

}