<?php
/**
 * Created by PhpStorm.
 * User: kamil
 * Date: 23.03.20
 * Time: 17:59
 */

namespace SimplifiedMagento\FirstModule\Model;

use SimplifiedMagento\FirstModule\Api\Color;
use SimplifiedMagento\FirstModule\Api\Size;

class Book
{
    protected $color;
    protected $size;
    public function __construct(Color $color, Size $size)
    {
        $this->color = $color;
        $this->size = $size;
    }

    public function getPencilType()
    {
        return "Our Book has " . $this->color->getColor() . " color and " . $this->size->getSize() . " size.";
    }
}