<?php
/**
 * Created by PhpStorm.
 * User: kamil
 * Date: 23.03.20
 * Time: 18:09
 */

namespace SimplifiedMagento\FirstModule\Model;

use SimplifiedMagento\FirstModule\Api\Brightness;

class Low implements Brightness
{
    public function getBrightness()
    {
        return "Low";
    }
}