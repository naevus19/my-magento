<?php
/**
 * Created by PhpStorm.
 * User: kamil
 * Date: 28.03.20
 * Time: 20:45
 */

namespace SimplifiedMagento\FirstModule\Model;


class HeavyService
{
    public function __construct()
    {
        echo "HeavyService has been instantiated </br>";
    }
    public function printHeavyServiceMessage() {
        echo "message from HeavyService class";
    }
}