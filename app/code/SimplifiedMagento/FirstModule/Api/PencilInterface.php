<?php
/**
 * Created by PhpStorm.
 * User: kamil
 * Date: 23.03.20
 * Time: 14:57
 */
namespace SimplifiedMagento\FirstModule\Api;

interface PencilInterface {

    public function  getPencilType();
}