<?php
/**
 * Created by PhpStorm.
 * User: kamil
 * Date: 23.03.20
 * Time: 18:04
 */

namespace SimplifiedMagento\FirstModule\Api;


interface Size
{
    public function getSize();
}