<?php
/**
 * Created by PhpStorm.
 * User: kamil
 * Date: 24.03.20
 * Time: 16:59
 */

namespace SimplifiedMagento\FirstModule\Api;


interface Brightness
{
    public function getBrightness();
}