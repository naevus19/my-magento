<?php

namespace SimplifiedMagento\FirstModule\Controller\Page;

use Magento\Framework\App\Action\Context;
//use Magento\Framework\App\ResponseInterface;
use SimplifiedMagento\FirstModule\Api\PencilInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use SimplifiedMagento\FirstModule\Model\PencilFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\App\Request\Http;
use SimplifiedMagento\FirstModule\Model\HeavyService;

class HelloWorld extends \Magento\Framework\App\Action\Action
{
    protected $pencilInterface;
    protected $pencilFactory;
    protected $productRepository;
    protected $productFactory;
    protected $_eventManager;
    protected $http;
    protected $heavyService;

    public function __construct(Context $context,
                                Http $http,
                                HeavyService $heavyService,
                                PencilInterface $pencilInterface,
                                PencilFactory $pencilFactory,
                                ProductRepositoryInterface $productRepository,
                                ProductFactory $productFactory,
                                ManagerInterface $_eventManager)
    {
        $this->http = $http;
        $this->heavyService = $heavyService;
        $this->pencilInterface = $pencilInterface;
        $this->pencilFactory = $pencilFactory;
        $this->productRepository = $productRepository;
        $this->productFactory = $productFactory;
        $this->_eventManager = $_eventManager;
        parent::__construct($context);
    }
    public function execute()
    {
        $id = $this->http->getParam('id', 0);
        if ($id === 1) {
            $this->heavyService->printHeavyServiceMessage();
        } else {
            echo "Heavy service not used";
        }

    }
}