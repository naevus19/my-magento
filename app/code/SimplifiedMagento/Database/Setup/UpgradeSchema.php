<?php
/**
 * Created by PhpStorm.
 * User: kamil
 * Date: 09.04.20
 * Time: 19:19
 */

namespace SimplifiedMagento\Database\Setup;


use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{

    /**
     * Upgrades DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), "0.1.1", "<")) {
            $setup->getConnection()->addColumn(
                $setup->getTable('affiliate_member'),
                'phone_number',
                ['nullable' => false, 'type' => Table::TYPE_TEXT, 'comment' => 'PHONE NUMBER OF MEMBER']
                );
        }
//        if (version_compare($context->getVersion(), '0.1.1', '<')) {
//            $setup->getConnection()->addColumn(
//                $setup->getTable('affiliate_member'),
//                'category_depth',
//                [
//                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
//                    'length' => 10,
//                    'nullable' => true,
//                    'comment' => 'Category Depth'
//                ]
//            );
//        }

        $setup->endSetup();
    }
}