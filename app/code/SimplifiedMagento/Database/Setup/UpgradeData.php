<?php
/**
 * Created by PhpStorm.
 * User: kamil
 * Date: 09.04.20
 * Time: 19:19
 */

namespace SimplifiedMagento\Database\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

class UpgradeData implements UpgradeDataInterface
{

    /**
     * Upgrades data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), "0.1.3", "<")) {
            $setup->getConnection()->insert(
                $setup->getTable('affiliate_member'),
                ['name' => 'Ade', 'status' => true, 'address' => 'Lubuska 113/2', 'phone_number' => '998773663']
            );
        }

        $setup->endSetup();
    }
}