<?php
/**
 * Created by PhpStorm.
 * User: kamil
 * Date: 26.04.20
 * Time: 10:20
 */

namespace SimplifiedMagento\Database\Controller\Page;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use SimplifiedMagento\Database\Model\AffiliateMemberFactory;
use SimplifiedMagento\Database\Model\ResourceModel\AffiliateMember;

class Index extends Action
{
    protected $affiliateMemberFactory;
    public function __construct(Context $context, AffiliateMemberFactory $affiliateMemberFactory)
    {
        $this->affiliateMemberFactory = $affiliateMemberFactory;
        parent::__construct($context);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $affiliateMember = $this->affiliateMemberFactory->create();
//        $member = $affiliateMember->load(4);
//        $member->delete();
//        var_dump($member->getData());
//        $affiliateMember->addData(
//            ['name' => 'Kamil', 'address' => 'Litewska 12/10', 'status' => false, 'phone_number' => '537453955']
//        );
//        $affiliateMember->save();

        $collection = $affiliateMember->getCollection()
            ->addFieldToSelect(['name', 'status', 'phone_number'])
            ->addFieldToFilter('status', array('neq'=>false));
        foreach ($collection as $item) {
            print_r($item->getData());
            echo '<br/>';
        }
    }
}