<?php
/**
 * Created by PhpStorm.
 * User: kamil
 * Date: 27.04.20
 * Time: 10:36
 */

namespace SimplifiedMagento\Database\Model;

use Magento\Framework\Api\SearchCriteriaInterface;
use SimplifiedMagento\Database\Api\AffiliateMemberRepositoryInterface;
use SimplifiedMagento\Database\Model\ResourceModel\AffiliateMember\CollectionFactory;
use SimplifiedMagento\Database\Model\ResourceModel\AffiliateMember;
use SimplifiedMagento\Database\Model\AffiliateMemberFactory;
use SimplifiedMagento\Database\Api\Data\AffiliateMemberSearchInterfaceFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessor;

class AffiliateMemberRepository implements AffiliateMemberRepositoryInterface
{
    private $collectionFactory;
    private $affiliateMemberFactory;
    private $affiliateMember;
    private $affiliateMemberSearchInterfaceFactory;
    private $collectionProcessor;

    public function __construct(CollectionFactory $collectionFactory,
                                AffiliateMemberFactory $affiliateMemberFactory,
                                AffiliateMember $affiliateMember,
                                AffiliateMemberSearchInterfaceFactory $affiliateMemberSearchInterfaceFactory,
                                CollectionProcessor $collectionProcessor)
    {
        $this->collectionFactory = $collectionFactory;
        $this->affiliateMemberFactory = $affiliateMemberFactory;
        $this->affiliateMember = $affiliateMember;
        $this->affiliateMemberSearchInterfaceFactory = $affiliateMemberSearchInterfaceFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @return \SimplifiedMagento\Database\Api\Data\AffiliateMemberInterface[]
     */
    public function getList()
    {
        return $this->collectionFactory->create()->getItems();
    }

    /**
     * @param int $id
     * @return \SimplifiedMagento\Database\Api\Data\AffiliateMemberInterface
     */

    public function getAffiliateMemberById($id)
    {
        $member = $this->affiliateMemberFactory->create();
        return $member->load($id);
    }

    /**
     * @param int $id
     * @return string
     */

    public function deleteAffiliateMemberById($id)
    {
        $member = $this->affiliateMemberFactory->create()->load($id);
        $member->delete();
        return "usunięto record";
    }

    public function saveAffiliateMember(\SimplifiedMagento\Database\Api\Data\AffiliateMemberInterface $member)
    {
        if ($member->getId() === null) {
            $this->affiliateMember->save($member);
            return $member;
        } else {
            $newMember = $this->affiliateMemberFactory->create()->load($member->getId());
            foreach ($member->getData() as $key => $value) {
                $newMember->setData($key, $value);
            }
            $this->affiliateMember->save($newMember);
            return $newMember;
        }
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return \SimplifiedMagento\Database\Api\Data\AffiliateMemberSearchInterface
     */

    public function getSearchResultsList(SearchCriteriaInterface $searchCriteria) {
        $collection = $this->affiliateMemberFactory->create()->getCollection();
        $this->collectionProcessor->process($searchCriteria, $collection);
        $searchResult = $this->affiliateMemberSearchInterfaceFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $searchResult->setItems($collection->getData());
        $searchResult->setTotalCount($collection->getSize());
        return $searchResult;
    }
}