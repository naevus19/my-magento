<?php
/**
 * Created by PhpStorm.
 * User: kamil
 * Date: 27.04.20
 * Time: 07:57
 */

namespace SimplifiedMagento\Database\Model\ResourceModel\AffiliateMember;


use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use SimplifiedMagento\Database\Model\ResourceModel\AffiliateMember as AffiliateMemberResource;
use SimplifiedMagento\Database\Model\AffiliateMember;

class Collection extends AbstractCollection
{
    public function _construct()
    {
        parent::_construct();
        $this->_init(AffiliateMember::class, AffiliateMemberResource::class);
    }

}