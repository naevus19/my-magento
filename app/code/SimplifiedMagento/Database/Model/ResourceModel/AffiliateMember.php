<?php

/**
 * Created by PhpStorm.
 * User: kamil
 * Date: 26.04.20
 * Time: 08:35
 */

namespace SimplifiedMagento\Database\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class AffiliateMember extends AbstractDb
{

    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('affiliate_member', 'entity_id');
    }


}