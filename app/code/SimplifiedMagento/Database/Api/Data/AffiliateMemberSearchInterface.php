<?php
/**
 * Created by PhpStorm.
 * User: kamil
 * Date: 29.04.20
 * Time: 07:54
 */

namespace SimplifiedMagento\Database\Api\Data;


use Magento\Framework\Api\SearchResultsInterface;

interface AffiliateMemberSearchInterface extends SearchResultsInterface
{
    /**
     * @return \Magento\Framework\Api\ExtensibleDataInterface[]
     */

    public function getItems();

    /**
     * @param array $items
     * @return SearchResultsInterface
     */

    public function setItems(array $items);

}