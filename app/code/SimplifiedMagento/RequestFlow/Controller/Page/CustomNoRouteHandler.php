<?php
/**
 * Created by PhpStorm.
 * User: kamil
 * Date: 06.04.20
 * Time: 19:15
 */

namespace SimplifiedMagento\RequestFlow\Controller\Page;


class CustomNoRouteHandler implements \Magento\Framework\App\Router\NoRouteHandlerInterface
{
    public function process(\Magento\Framework\App\RequestInterface $request)
    {

        $request->setRouteName('noroutefound')->setControllerName('page')->setActionName('customnoroute');

        return true;
    }
}