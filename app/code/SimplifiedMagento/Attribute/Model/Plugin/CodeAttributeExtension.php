<?php
/**
 * Created by PhpStorm.
 * User: kamil
 * Date: 07.05.20
 * Time: 16:39
 */

namespace SimplifiedMagento\Attribute\Model\Plugin;

use SimplifiedMagento\Database\Api\AffiliateMemberRepositoryInterface;
use \SimplifiedMagento\Database\Api\Data\AffiliateMemberExtensionFactory;
use SimplifiedMagento\Database\Api\Data\AffiliateMemberInterface;
use SimplifiedMagento\Database\Model\AffiliateMember;
use \SimplifiedMagento\Database\Model\AffiliateMemberRepository;

class CodeAttributeExtension
{
    protected $extensionFactory;

    public function __construct(AffiliateMemberExtensionFactory $extensionFactory)
    {
        $this->extensionFactory = $extensionFactory;
    }

    public function afterGetAffiliateMemberById
    (
        AffiliateMemberRepositoryInterface $subject,
        AffiliateMember $entity
    ) {
        $entity->setCustomAttribute('sample', "Code #" . $entity->getId());
        $extensionAttributes = $entity->getExtensionAttributes();

        if (empty($extensionAttributes)) {
            $extensionAttributes = $this->extensionFactory->create();
        }
        $extensionAttributes->setSample("Code #" . $entity->getId());
        $entity->setExtensionAttributes($extensionAttributes);

        return $entity;
    }

}