<?php
/**
 * Created by PhpStorm.
 * User: kamil
 * Date: 06.05.20
 * Time: 15:51
 */

namespace SimplifiedMagento\Attribute\Model\Config;


use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class Options extends AbstractSource
{

    /**
     * Retrieve All options
     *
     * @return array
     */
    public function getAllOptions()
    {
        $this->_options = [
            ['label' => __('Gold'), 'value' => 'gold'],
            ['label' => __('Silver'), 'value' => 'silver'],
            ['label' => __('Bronze'), 'value' => 'bronze'],
        ];
        return $this->_options;
    }
}